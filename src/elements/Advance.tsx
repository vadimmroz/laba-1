import React, {useState} from 'react';
import {Button, Card, TextField} from "@mui/material";
import '../App.css';
import Header from "./Header";
// @ts-ignore
const Advance = ({endpoint}) => {
    const [all, setAll] = useState('')
    const [exact, setExect] = useState('')
    const [any, setAny] = useState('')
    const [none, setNone] = useState('')
    const handleAll = (e: string) => {
        setAll(e)
    }
    const handleExact = (e: string) => {
        setExect(e)
    }
    const handleAny = (e: string) => {
        setAny(e)
    }
    const handleNone = (e: string) => {
        setNone(e)
    }
    return (
        <>
            <Header/>
            <Card className='card advance'>
                <TextField id="standard-basic" value={all} label="усі ці слова:" variant="standard" className='input'
                           onChange={(e) => handleAll(e.target.value)}/>
                <TextField id="standard-basic" value={exact} label="точне слово або фразу:" variant="standard"
                           className='input' onChange={(e) => handleExact(e.target.value)}/>
                <TextField id="standard-basic" value={any} label="будь-яке з цих слів:" variant="standard"
                           className='input' onChange={(e) => handleAny(e.target.value)}/>
                <TextField id="standard-basic" value={none} label="жодне з цих слів:" variant="standard"
                           className='input' onChange={(e) => handleNone(e.target.value)}/>
                <a href={endpoint + (all.length > 0 ? all : '') + (exact.length > 0 ? ' ' + exact : '') + (any.length > 0 ? ' "' + any + '"' : '') + (none.length > 0 ? ' -' + none : '')}><Button
                    variant='contained' className='button'>Пошук</Button></a>
            </Card>
        </>
    );
};

export default Advance;