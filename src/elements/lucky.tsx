import React from 'react';
import {Button, Card} from "@mui/material";
import '../App.css';
import Header from "./Header";
// @ts-ignore
const Lucky = () => {

    return (
        <>
            <Header/>
            <Card className='card'>
                <a href={'https://www.google.com/doodles'}><Button variant='contained' className='button'>Мені пощастить</Button></a>
            </Card>
        </>
    );
};

export default Lucky;