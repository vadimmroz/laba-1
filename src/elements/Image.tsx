import React from 'react';
import {Button, Card, TextField} from "@mui/material";
import '../App.css';
import Header from "./Header";
// @ts-ignore
const Image = ({retake, endpoint, params}) => {

    return (
        <>
            <Header/>
            <Card className='card'>
                <TextField id="standard-basic" label="Ведіть текст"  value={params} variant="standard" className='input' onChange={(e)=>retake(e.target.value)} />
                <a href={endpoint + params + '&tbm=isch'}><Button variant='contained' className='button'>Пошук</Button></a>
            </Card>
        </>
    );
};

export default Image;