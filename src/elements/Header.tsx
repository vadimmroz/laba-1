import React from 'react';
import '../App.css';
import {NavLink} from "react-router-dom";
import {Typography, Button, Menu} from "@mui/material";
import {useLocation} from "react-router";
import MenuIcon from '@mui/icons-material/Menu';

const Header = () => {
    const location = useLocation()
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    // @ts-ignore
    return (
        <header>
            <nav>
                <NavLink to='/search'>
                    <Typography variant='h3' className={ location.pathname === '/search' ? 'active' : ''}>
                        Search
                    </Typography>
                </NavLink>
                <NavLink to='/image'>
                    <Typography variant='h3' className={ location.pathname === '/image' ? 'active' : ''}>
                        Image
                    </Typography>
                </NavLink>
                <NavLink to='/lucky'>
                    <Typography variant='h3' className={ location.pathname === '/lucky' ? 'active' : ''}>
                        Lucky
                    </Typography>
                </NavLink>
                <NavLink to='/advance'>
                    <Typography variant='h3' className={ location.pathname === '/advance' ? 'active' : ''}>
                        Advance
                    </Typography>
                </NavLink>
            </nav>
            <div id='menu'>
                <Button
                    id="basic-button"
                    aria-controls={open ? 'basic-menu' : undefined}
                    aria-haspopup="true"
                    aria-expanded={open ? 'true' : undefined}
                    onClick={handleClick}
                >
                    <MenuIcon/>
                </Button>
                <Menu
                    id="basic-menu"
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}
                    MenuListProps={{
                        'aria-labelledby': 'basic-button',
                    }}
                >
                    <NavLink onClick={handleClose} to='/search'>
                        <Typography variant='h4' className={ location.pathname === '/search' ? 'active' : ''}>
                            Search
                        </Typography>
                    </NavLink>
                    <NavLink onClick={handleClose} to='/image'>
                        <Typography variant='h4' className={ location.pathname === '/image' ? 'active' : ''}>
                            Image
                        </Typography>
                    </NavLink>
                    <NavLink onClick={handleClose} to='/lucky'>
                        <Typography variant='h4' className={ location.pathname === '/lucky' ? 'active' : ''}>
                            Lucky
                        </Typography>
                    </NavLink>
                    <NavLink onClick={handleClose} to='/advance'>
                        <Typography variant='h4' className={ location.pathname === '/advance' ? 'active' : ''}>
                            Advance
                        </Typography>
                    </NavLink>
                </Menu>
            </div>

        </header>
    );
};

export default Header;