import React, {useState} from 'react';
import './App.css';
import {Routes, Route, Navigate} from "react-router-dom";
import Search from "./elements/Search";
import Image from "./elements/Image";
import Lucky from "./elements/lucky";
import Advance from "./elements/Advance";

const endpoint = 'https://www.google.com/search?q='
function App() {
    const [params, setParams] = useState('')

    const retake = (e: string)=>{
        setParams(e)
    }
    // @ts-ignore
    return (
        <>
        <Routes>
            <Route path='*' element={<Navigate to='/search'/>}/>
            <Route path='search' element={<Search key={1} retake={retake} endpoint={endpoint} params={params}/>}/>
            <Route path='image' element={<Image key={2} retake={retake} endpoint={endpoint} params={params}/>}/>
            <Route path='lucky' element={<Lucky key={3}/>} />
            <Route path='advance' element={<Advance endpoint={endpoint}/>}/>
        </Routes>
        </>
        //made by Vadim Zaloznyi
    );
}

export default App;
